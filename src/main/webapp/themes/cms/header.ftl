<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>${SYS_SITENAME}</title>

    <!-- Bootstrap core CSS -->
    <link href="${TEMPLATE_PATH}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${TEMPLATE_PATH}/css/theme.css" rel="stylesheet">
    <link href="${TEMPLATE_PATH}/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="${TEMPLATE_PATH}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="${TEMPLATE_PATH}/css/flexslider.css"/>
    <link href="${TEMPLATE_PATH}/assets/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <link href="${TEMPLATE_PATH}/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />

    <link rel="stylesheet" href="${TEMPLATE_PATH}/assets/revolution_slider/css/rs-style.css" media="screen">
    <link rel="stylesheet" href="${TEMPLATE_PATH}/assets/revolution_slider/rs-plugin/css/settings.css" media="screen">
    <link rel="stylesheet" href="${TEMPLATE_PATH}/assets/SyntaxHighlighter/styles/shCoreDefault.css" media="screen">

    <!-- Custom styles for this template -->
    <link href="${TEMPLATE_PATH}/css/style.css" rel="stylesheet">
    <link href="${TEMPLATE_PATH}/css/style-responsive.css" rel="stylesheet" />
    <script src="${TEMPLATE_PATH}/js/jquery.js"></script>
    <script src="${TEMPLATE_PATH}/js/jquery.form.js"></script>
    <script src="${TEMPLATE_PATH}/js/jquery.validate.js"></script>  
    

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="${TEMPLATE_PATH}/js/html5shiv.js"></script>
      <script src="${TEMPLATE_PATH}/js/respond.min.js"></script>
    <![endif]-->
	<script type="text/javascript">
		window.TEMPLATE_PATH = "${TEMPLATE_PATH}";
	</script>    
  </head>

  <body>
     <!--header start-->
    <header class="header-frontend">
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="${basePath}/">${SYS_SITENAME}<span style="font-size: 14px; margin-left: 10px;">${SYS_SITEDESC}</span></a>
                </div>            
            </div>
        </div>
    </header>
    <!--header end-->