	<footer class="footer">
         <div class="container">
             <div class="row">
                 <div class="col-lg-5 col-sm-5">
                     Powered by <a href="http://cms.shishuo.com">师说CMS</a> | UI Designed by <a href="http://www.shishuo.com">师说</a>
                 </div>
                 <div class="col-lg-7 col-sm-7 pull-right" style="text-align: right;">
                 	
                 </div>
             </div>
         </div>
    </footer>
     
  	<!-- js placed at the end of the document so the pages load faster -->
  	
    <script src="${TEMPLATE_PATH}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${TEMPLATE_PATH}/js/hover-dropdown.js"></script>
    <script defer src="${TEMPLATE_PATH}/js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="${TEMPLATE_PATH}/assets/bxslider/jquery.bxslider.js"></script>
    <script type="text/javascript" src="${TEMPLATE_PATH}/assets/SyntaxHighlighter/scripts/shCore.js"></script>
    <script type="text/javascript" src="${TEMPLATE_PATH}/assets/SyntaxHighlighter/scripts/shAutoloader.js"></script>

    <script src="${TEMPLATE_PATH}/js/jquery.easing.min.js"></script>
    <script src="${TEMPLATE_PATH}/js/link-hover.js"></script>
    <script src="${TEMPLATE_PATH}/assets/Slides-SlidesJS-3/source/jquery.slides.js"></script>
  	<script src="${TEMPLATE_PATH}/assets/Slides-SlidesJS-3/source/jquery.slides.min.js"></script>
    
    <!--common script for all pages-->
    <script src="${TEMPLATE_PATH}/js/common-scripts.js"></script>

  </body>
</html>